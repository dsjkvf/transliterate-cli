## Transliteration for the command line, et al

Here is a Python script that transliterates text, which was mistakenly entered using the wrong keyboard layout, to a correct one. Conversion is bi-directional only at the moment, and for now only two scripts are supported, Latin and Cyrillic. Keyboard layouts in question are described in plain text files (like `keyb-phonetic.txt`) and the common usage is the following:

    transliterate-cli.py <keyboard layout to use> <text to convert>
    # the layout is passed like `phonetic` or `normal`

One of possible uses for this script is to employ it with system services (like those of OS X, think Automator) or via another software (think Apple Script).
